# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils import timezone

from django import forms

from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.mail import send_mail

from models import Grupa
from models import CanvasUser
from django.core.cache import cache

import memoryZip

import getpass
import requests
from bs4 import BeautifulSoup

import hashlib





class CanvasForm(forms.Form):
    login = forms.EmailField()
    haslo = forms.CharField(max_length=32, widget=forms.PasswordInput)






def canvasLogin(request):
    if request.method == 'POST':
        forma = CanvasForm(request.POST)
        if forma.is_valid():
            canvaslogin = forma.cleaned_data['login']
            canvaspassword = forma.cleaned_data['haslo']
            u = CanvasUser(login=canvaslogin, password=canvaspassword, session_time=timezone.now())
            u.save()
            request.session['canvasuser'] = canvaslogin
            return HttpResponseRedirect(reverse('komunikat', args=('ok', 'utworzono_canvas',)))
    else:
        form = CanvasForm()
        return render(request, 'canvas/canvaslogin.html', {
        'forma': form, })

def lenghtenSession(request):

    if 'canvasuser' in request.session:
        if request.session['canvasuser'] is not None:
            u = CanvasUser.objects.filter(login=request.session['canvasuser'])[0]
            u.session_time = timezone.now()
            u.save()
            return HttpResponseRedirect(reverse('komunikat', args=('ok', 'sesja_przedluzona',)))

        else:
            return HttpResponseRedirect(reverse('komunikat', args=('blad','wrong', )))
    else:
        return HttpResponseRedirect(reverse('komunikat', args=('blad','wrong', )))





def komunikat(request, typ, message):
    tresc = ''
    if message == 'login_ok':
        tresc = 'Zalogowano pomyslnie'
    elif message == 'logout_ok':
        tresc = 'Wylogowano pomyslnie'
    elif message == 'canvas_set':
        tresc = 'Zapisano haslo canvas'
    elif message == 'not_logged_in':
        tresc = 'Nie jestes zalogowany'
    elif message == 'wpis_ok':
        tresc = 'Twoj wpis dodano pomyslnie'
    elif message == 'user_exists':
        tresc = 'Taki uzytkownik juz istnieje'
    elif message == 'pwd_not_match':
        tresc = 'Podane hasla nie sa identyczne'
    elif message == 'register_ok':
        tresc = 'Rejestracja poprawna. E-mail aktywacyjny zostal wyslany na podany adres.'
    elif message == 'not_activated':
        tresc = 'Konto nie jest aktywne'
    elif message == 'activated':
        tresc = 'Twoje konto zostalo aktywowane. Mozesz sie zalogowac.'
    elif message == 'edit_ok':
        tresc = 'Edycja postu przebiegla pomyslnie.'
    elif message == 'cant_edit':
        tresc = 'Musi minac conajmniej 10 minut od ostatniej edycji lub dodania wpisu'
    elif message == 'sesja_wygasla':
        tresc = 'Twoja sesja wygasla'
    elif message == 'utworzono_canvas':
        tresc = 'Sesja canvas zostala utworzona'
    elif message == 'sesja_przedluzona':
        tresc = 'Przedluzono sesje'
    else:
        tresc = 'Cos chyba kombinujesz'

    if typ == 'ok' or typ == 'blad':
        return render(request, 'canvas/komunikat.html',
            {
                'typ': typ,
                'tresc': tresc,
                'zalogowany': request.session['zalogowany'],
            })
    else:
        return render(request, 'canvas/komunikat.html',
            {
                'typ': 'blad',
                'tresc': tresc,
                'zalogowany': request.session['zalogowany'],
            })


def przedmioty(request):

    username = None
    if 'canvasuser' in request.session:
        username = request.session['canvasuser']

    if username is not None:
        u = CanvasUser.objects.filter(login=username)

        if u.count() == 0:
            return []

        u = u[0]

        url = {
            'login': 'https://canvas.instructure.com/login',
            'grades': 'https://canvas.instructure.com/courses/838324/grades',
            'przedmioty': 'https://canvas.instructure.com/courses'
        }

        username = u.login
        password = u.password
        payload = {
            'pseudonym_session[unique_id]': username,
            'pseudonym_session[password]': password
        }

        # Logowanie
        session = requests.session()
        session.post(url['login'], data=payload)
        # Pobranie strony z ocenami
        response = session.get(url['przedmioty'])
        #response.status_code
        #response.headers['content-type']
        # Odczyt strony
        html = response.text
        # Parsowanie strony
        parsed = BeautifulSoup(html, 'html5lib')
        # Scraping
        przedmioty = []
        linki_przedmiotow = []
        #blok = parsed.find('div', {'id': 'content'})

        kursy_li = parsed.find_all('li', {'class': 'active'})

        for element in kursy_li:
            link = element.find_all('a')
            for l in link:
                linki_przedmiotow.append(l['href'])

            kursy_napisy = element.find('span', {'class': 'name ellipsis'})
            for kurs in kursy_napisy:
                przedmioty.append(kurs)

        return (przedmioty, linki_przedmiotow)

    else:
        return []


def dwnlzip(request, id):


    #najpierw sprawdzic sesje
    username = None
    zip = None
    if 'canvasuser' in request.session:
        username = request.session['canvasuser']

    #jeszcze raz pobranie listy przedmiotow



    #datrtregregreg
    if username is not None:
        zip = cache.get(username)
        (prz, li) = przedmioty(request)


        link = "https://canvas.instructure.com" + li[int(id)] + "/grades"
        u = CanvasUser.objects.filter(login=username)

        if u.count() == 0:
            return []

        u = u[0]



        url = {
            'login': 'https://canvas.instructure.com/login',
            'grades': 'https://canvas.instructure.com/courses/838324/grades',
            'przedmioty': 'https://canvas.instructure.com/courses'
        }

        username = u.login
        password = u.password

        payload = {
            'pseudonym_session[unique_id]': username,
            'pseudonym_session[password]': password
        }

        # Logowanie
        session = requests.session()
        session.post(url['login'], data=payload)
        # Pobranie strony z ocenami
        response = session.get(link)
        #response.status_code
        #response.headers['content-type']
        # Odczyt strony
        html = response.text
        # Parsowanie strony
        parsed = BeautifulSoup(html, 'html5lib')

        oceny_tuple = []

        nazwy = []
        kategorie = []
        daty = []
        oceny = []

        #kazdy wiersz tabeli
        wiersze = parsed.find_all('tr', {'class': 'student_assignment assignment_graded editable'})

        #w kazdym wierszu
        for w in wiersze:
            #znalezc header
            header = w.find('th')
            if header is not None:
                a = header.find('a')
                if a is not None:
                    nazwy.append(a.text)
                else:
                    nazwy.append('')

                #w headerze jeszcze jest kategoria zadania
                kat = header.find('div', {'class': 'context'})
                if kat is not None:
                    kategorie.append(kat.text)
                else:
                    kategorie.append('')

            #znalezc termin oddania
            t = w.find('td', {'class': 'due'})
            if t is not None:
                daty.append(t.text.strip())
            else:
                daty.append('')

            #znalezc punkty za zadanie
            pkt = w.find('td', {'class': 'assignment_score'})
            if pkt is not None:
                grade = pkt.find('span', {'class': 'grade'})
                if grade is not None:
                    oceny.append(grade.text[100:].strip())
            else:
                oceny.append('')


        #przygotowanie tupli do posortowania

        if zip is None:
        #generuj zipa
            zip = memoryZip.InMemoryZip()
            txt=""
            for i in range(0, len(nazwy)):
                txt += kategorie[i] + ', ' + nazwy[i] + ', ' + daty[i] + ', ' + oceny[i] + '/n'
            zip.append('oceny.txt', txt.encode('utf-8'))
            cache.set(username,zip)

    #dsasdadsadasd
    response = HttpResponse(mimetype='application/zip')
    response['Content-Disposition'] = 'filename=logs_%s.zip'
    response.write(zip.read())
    return response

def showgrades(request, id):


    #najpierw sprawdzic sesje
    username = None
    zip = None
    if 'canvasuser' in request.session:
        username = request.session['canvasuser']

    #jeszcze raz pobranie listy przedmiotow



    #datrtregregreg
    if username is not None:
        zip = cache.get(username)
        (prz, li) = przedmioty(request)


        link = "https://canvas.instructure.com" + li[int(id)] + "/grades"
        u = CanvasUser.objects.filter(login=username)

        if u.count() == 0:
            return []

        u = u[0]



        url = {
            'login': 'https://canvas.instructure.com/login',
            'grades': 'https://canvas.instructure.com/courses/838324/grades',
            'przedmioty': 'https://canvas.instructure.com/courses'
        }

        username = u.login
        password = u.password

        payload = {
            'pseudonym_session[unique_id]': username,
            'pseudonym_session[password]': password
        }

        # Logowanie
        session = requests.session()
        session.post(url['login'], data=payload)
        # Pobranie strony z ocenami
        response = session.get(link)
        #response.status_code
        #response.headers['content-type']
        # Odczyt strony
        html = response.text
        # Parsowanie strony
        parsed = BeautifulSoup(html, 'html5lib')

        oceny_tuple = []

        nazwy = []
        kategorie = []
        daty = []
        oceny = []

        #kazdy wiersz tabeli
        wiersze = parsed.find_all('tr', {'class': 'student_assignment assignment_graded editable'})

        #w kazdym wierszu
        for w in wiersze:
            #znalezc header
            header = w.find('th')
            if header is not None:
                a = header.find('a')
                if a is not None:
                    nazwy.append(a.text)
                else:
                    nazwy.append('')

                #w headerze jeszcze jest kategoria zadania
                kat = header.find('div', {'class': 'context'})
                if kat is not None:
                    kategorie.append(kat.text)
                else:
                    kategorie.append('')

            #znalezc termin oddania
            t = w.find('td', {'class': 'due'})
            if t is not None:
                daty.append(t.text.strip())
            else:
                daty.append('')

            #znalezc punkty za zadanie
            pkt = w.find('td', {'class': 'assignment_score'})
            if pkt is not None:
                grade = pkt.find('span', {'class': 'grade'})
                if grade is not None:
                    oceny.append(grade.text[100:].strip())
            else:
                oceny.append('')


        #przygotowanie tupli do posortowania

        if zip is None:
            #generuj zipa
            zip = memoryZip.InMemoryZip()
            txt=""
            for i in range(0, len(nazwy)):
                txt += kategorie[i] + ', ' + nazwy[i] + ', ' + daty[i] + ', ' + oceny[i] + '\n'
            zip.append('oceny.txt', txt.encode('utf-8'))
            cache.set(username,zip)


        for i in range(0, len(nazwy)):
            oceny_tuple.append((kategorie[i], nazwy[i], daty[i], oceny[i]))

        #sortowanie tupla
        sortowany = sorted(oceny_tuple, key=lambda wpis: wpis[0])

        user_time = u.session_time
        current = timezone.now()
        dt = current - user_time



    return render(request, 'canvas/grades.html', {
        'zalogowany': request.session['zalogowany'],
        'user': request.session['user'],
        'wszystkie_wpisy': sortowany,
        'czas': 120 - dt.seconds,
        'przedmiot': id,
    })


def index(request):
    if not request.session.get('zalogowany'):
        request.session['zalogowany'] = 'False'
        request.session['user'] = None



    username=None
    if 'canvasuser' in request.session:
        username = request.session['canvasuser']


    if username is not None:
        u = CanvasUser.objects.filter(login=username)[0]
        aktualna = timezone.now()
        usera = u.session_time

        dt = aktualna - usera
        if dt.seconds >= 120:
            del request.session['canvasuser']
            u.delete()
            return HttpResponseRedirect(reverse('komunikat', args=('blad', 'sesja_wygasla',)))

        else:
            return render(request, 'canvas/index.html', {
            'zalogowany': request.session['zalogowany'],
            'user': request.session['user'],
            'wszystkie_wpisy': przedmioty(request),
            'czas': 120 - dt.seconds, })

    return render(request, 'canvas/index.html', {
        'zalogowany': request.session['zalogowany'],
        'user': request.session['user'],
        'wszystkie_wpisy': przedmioty(request),
    })