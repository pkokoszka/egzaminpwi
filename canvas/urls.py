
from django.conf.urls import patterns, url, include
from django.views.generic import TemplateView
from django.views.generic import ListView

import views



urlpatterns = patterns('',
        url(r'^$', views.index, name='index'),
        url(r'^komunikat/(?P<typ>\w+)/(?P<message>\w+)/$', views.komunikat, name='komunikat'),
        url(r'^showgrades/(?P<id>\w+)/$', views.showgrades, name='showGrades'),
        url(r'^canvasLogin/', views.canvasLogin, name='canvasLogin'),
        url(r'^lengthenSession/', views.lenghtenSession, name='lengthenSession'),
        url(r'^dwnlgrades/(?P<id>\w+)/$', views.dwnlzip, name='dwnlZip'),

)